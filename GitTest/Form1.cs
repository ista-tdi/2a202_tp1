﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GitTest
{
    public partial class Form1 : Form
    {

        private Database db;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Une instance de la "classe Database" est crée
            db = new Database();
        }

        private void btnConnecter_Click(object sender, EventArgs e)
        {
            db.Connecter();
            labelStatus.Text = db.connection.State.ToString();
        }

        private void btnDeconnecter_Click(object sender, EventArgs e)
        {
            db.Deconnecter();
            labelStatus.Text = db.connection.State.ToString();

        }
    }
}
