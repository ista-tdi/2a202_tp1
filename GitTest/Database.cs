﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Data;

namespace GitTest
{
    class Database
    {
        // Declaration des objets de SGBD
        public SqlConnection connection = new SqlConnection();
        public SqlCommand cmd = new SqlCommand();
        public SqlDataAdapter dataAdapter = new SqlDataAdapter();
        public DataTable dataTable;


        // Methodes
        public void Connecter()
        {
            connection.ConnectionString = "Data Source=DESKTOP-HFLAVK5;Initial Catalog=TP1;Integrated Security=True";
            
            if(connection.State.Equals(ConnectionState.Closed) ||
                connection.State.Equals(ConnectionState.Broken))
            {
                connection.Open();
            }
        }

        public void Deconnecter()
        {
            if (connection.State.Equals(ConnectionState.Open))
            {
                connection.Close();
            }
        }

    }
}
